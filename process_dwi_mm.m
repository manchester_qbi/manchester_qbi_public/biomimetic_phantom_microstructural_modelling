%% Script for processing microstructural modelling data

%% Add parent directory to path
parentDir=pwd;
addpath(parentDir)

%% Data information
pathToData=...
    {'./plga_stability_210316/Analysis_day7/'};
dataFile='data.mat';
noiseFile='ricianNoiseStd_diff.mat';
scanParamFile='scanParams.mat';
maskFileDir='diff_DEL12_del4_G_0_70_140_210/';
maskFile='mask';

%% Which which type of fitting - voxel-wise or averaging over all voxels
fitType='voxelwise'; %'voxelwise' or 'allVoxelsMean'

%% Use model with one diffusivity (Di=De: 'D') or two (Di~=De: 'DiDe')
diffModel='D';%'D','DiDe'

%% Include or exclude highest G, highest DELTA point?
highG_highDEL='include';%'exclude' or 'include' or 'excludeALLDEL45'

%% Scale variables?
scaling='unscaled';%'scaled' or 'unscaled'

%% Loop over days---------------------------------------------------------
for scanInd=1:numel(pathToData)
    clearvars -except pathToData dataFile noiseFile scanParamFile ...
        maskFileDir maskFile fitType diffModel highG_highDEL scaling scanInd
    
    %% Load scan parameters and dwi data
    scanParams=...
        mm_plga.loadRename(fullfile(pathToData{scanInd},scanParamFile));
    del12=mm_plga.loadRename(fullfile(pathToData{scanInd},...
        'diff_DEL12_del4_G_0_70_140_210/',dataFile));
    del23=mm_plga.loadRename(fullfile(pathToData{scanInd},...
        'diff_DEL23_del4_G_0_70_140_210/',dataFile));
    del45=mm_plga.loadRename(fullfile(pathToData{scanInd},...
        'diff_DEL45_del4_G_0_70_140_210/',dataFile));
    noiseLevel=mm_plga.loadRename(fullfile(pathToData{scanInd},...
        'diff_DEL12_del4_G_0_70_140_210/',noiseFile));
    saveDir=pathToData{scanInd};
    
    %% Sort data for fitting
    %  - Format - x,y,gradient,slice,direction,DELTA
    dataMatrix=mm_plga.sortModellingStructs(del12,del23,del45);
    
    %% Remove highest-G, highest-DELTA data from fitting, if needed
    switch highG_highDEL
        case 'exclude'
            dataMatrix(:,:,end,:,:,end)=...
                zeros(size(dataMatrix,1),size(dataMatrix,2),size(dataMatrix,4))+eps;
        case 'excludeALLDEL45'
            disp('!*** Excluding all DELTA=45 ms data ***!')
            for gind=1:size(dataMatrix,3)
                dataMatrix(:,:,gind,:,:,end)=...
                    zeros(size(dataMatrix,1),size(dataMatrix,2),size(dataMatrix,4))+eps;
            end
        case 'include'
            % Nothing to do
        otherwise
            error('!!! Unrecognised highG_highDEL')
    end
    
    %% Loop over bounds
    for fitBounds=1:2
        switch diffModel
            case 'D'
                rLwrLim=0.1e-6; % R lower limit (um)
                rUpprLim=25e-6; % R upper limit (um)
                dLwrLim=0.1e-9; % D lower limit (um^2/ms)
                dUpprLim=3e-9;  % D upper limit (um^2/ms)
                fLwrLim=0.01;   % f lower limit
                fUpprLim=1;     % f upper limit
                
                %% Generate matrix of values for fit initialisation
                rngSeed=134564;
                numInitialParams=100;
                radRange=[rLwrLim rUpprLim];
                diffRange=[dLwrLim dUpprLim];
                fRange=[fLwrLim fUpprLim];
                RandStream.setGlobalStream ...
                    (RandStream('mt19937ar','seed',rngSeed));
                for i=1:numInitialParams
                    initialParams(1,i)=...
                        (radRange(1) + (radRange(2)-radRange(1)).*rand(1,1));
                    initialParams(2,i)=...
                        (diffRange(1) + (diffRange(2)-diffRange(1)).*rand(1,1));
                    initialParams(3,i)=...
                        (fRange(1) + (fRange(2)-fRange(1)).*rand(1,1));
                    initialParams(4,i)=1;
                end
                initialParamSeed=initialParams;
                
                if fitBounds==1 % Fit all parameters
                    lwrBnd=[rLwrLim dLwrLim fLwrLim 1];
                    upprBnd=[rUpprLim dUpprLim fUpprLim 1];
                elseif fitBounds==2 % Fix diffusivity
                    try
                        waterMedianADC=...
                            mm_plga.loadRename(fullfile(pathToData{scanInd},...
                            '/diff_DEL12_del4_b150_500_1000/water_adcMLE_median.mat'));
                    catch
                        waterMedianADC=2.01e-3;
                        disp('****************')
                        disp('Free water ADC data not found - fixing to:')
                        disp(waterMedianADC)
                        disp('****************')
                    end
                    fixD=waterMedianADC./1e6; % convert to um^2/ms
                    lwrBnd=[rLwrLim fixD fLwrLim 1];
                    upprBnd=[rUpprLim fixD fUpprLim 1];
                else
                    error('!!! Invalid fitBounds')
                end
                
            case 'DiDe'
                %
                rLwrLim=0.1e-6; % R lower limit (um)
                rUpprLim=25e-6; % R upper limit (um)
                diLwrLim=0.1e-9; % Di lower limit (um^2/ms)
                diUpprLim=3e-9;  % Di upper limit (um^2/ms)
                deLwrLim=0.1e-9; % De lower limit (um^2/ms)
                deUpprLim=3e-9;  % De upper limit (um^2/ms)
                fLwrLim=0.01;   % f lower limit
                fUpprLim=1;     % f upper limit
                
                %% Generate matrix of values for fit initialisation
                rngSeed=134564;
                numInitialParams=100;
                radRange=[rLwrLim rUpprLim];
                diRange=[diLwrLim diUpprLim];
                deRange=[deLwrLim deUpprLim];
                fRange=[fLwrLim fUpprLim];
                RandStream.setGlobalStream ...
                    (RandStream('mt19937ar','seed',rngSeed));
                for i=1:numInitialParams
                    initialParams(1,i)=...
                        (radRange(1) + (radRange(2)-radRange(1)).*rand(1,1));
                    initialParams(2,i)=...
                        (di(1) + (diRange(2)-diRange(1)).*rand(1,1));
                    initialParams(2,i)=...
                        (de(1) + (deRange(2)-deRange(1)).*rand(1,1));
                    initialParams(4,i)=...
                        (fRange(1) + (fRange(2)-fRange(1)).*rand(1,1));
                end
                initialParamSeed=initialParams;
                
                if fitBounds==1 % Fit all parameters
                    lwrBnd=[rLwrLim diLwrLim deLwrLim fLwrLim];
                    upprBnd=[rUpprLim diUpprLim deUpprLim fUpprLim];
                elseif fitBounds==2
                    try
                        waterMedianADC=...
                            mm_plga.loadRename(fullfile(pathToData{scanInd},...
                            '/diff_DEL12_del4_b150_500_1000/water_adcMLE_median.mat'));
                    catch
                        waterMedianADC=2.01e-3;
                        disp('****************')
                        disp('Free water ADC data not found - fixing to:')
                        disp(waterMedianADC)
                        disp('****************')
                    end
                    fixD=waterMedianADC./1e6; % convert to um^2/ms
                    lwrBnd=[rLwrLim diLwrLim fixD fLwrLim];
                    upprBnd=[rUpprLim diUpprLim fixD fUpprLim];
                else
                    error('!!! Invalid fitBounds')
                end
                %
            otherwise
                error('!!! Unrecognised diffModel')
        end
        
        %% Generate strings for saving
        saveStr=strcat(fitType,'_',maskFile,'_','LB_',...
            num2str(lwrBnd(1)),'_',num2str(lwrBnd(2)),'_',...
            num2str(lwrBnd(3)),'_',num2str(lwrBnd(4)),'_UB_',...
            num2str(upprBnd(1)),'_',num2str(upprBnd(2)),'_',...
            num2str(upprBnd(3)),'_',num2str(upprBnd(4)));
        disp(saveStr);
        
        switch highG_highDEL
            case 'include'
                highG_highDEL_str=''; %'default' option so leave unnamed
            case 'exclude'
                highG_highDEL_str='EXCLUDE_G210_DEL45';
            case 'excludeALLDEL45'
                highG_highDEL_str='EXCLUDE_ALL_DEL45';
        end
        
        subDir=strcat(saveDir,'mm_seed',num2str(rngSeed),'_',...
            num2str(numInitialParams),'initialVals_',diffModel,...
            '/',saveStr,'_',highG_highDEL_str,'_',scaling,'/');
        
        %% Create directory in which to save output
        if ~exist(subDir,'dir')
            mkdir(subDir)
        else
            disp('Directory already exists')
        end
        
        %% For consistency with previous code, replicate data along fifth
        %  dimension, so that it appears there are 3 directions
        %  This simply means matrix dimensions are consistent (so
        %  squeeze() doesn't collape more than one dimension)
        dataMatrixRep=cat(5,dataMatrix,dataMatrix,dataMatrix);
        
        %% Deal with scaling if necessary
        switch scaling
            case 'scaled' % scale tissue properties, bounds and scan parameters
                switch diffModel
                    case 'D'
                        modelParamScalings=[1e6 1e9 1 1];
                    case 'DiDe'
                        modelParamScalings=[1e6 1e9 1e9 1];
                end
                initialParamSeedScaled=...
                    bsxfun(@times,initialParamSeed,modelParamScalings');
                lwrBndScaled=lwrBnd.*modelParamScalings;
                upprBndScaled=upprBnd.*modelParamScalings;
                scanParamsScaled=bsxfun(@times,scanParams,[1e3 1e3 1e3 1e-12]);
            case 'unscaled'
                % Reassign, so correct values are used on each loop
                initialParamSeedScaled=initialParamSeed;
                lwrBndScaled=lwrBnd;
                upprBndScaled=upprBnd;
                scanParamsScaled=scanParams;
        end
        
        %% Fitting
        pathToPhntmMask=...
            fullfile(pathToData{scanInd},maskFileDir,maskFile);
        phntmMask=mm_plga.loadRename(pathToPhntmMask);
        
        switch fitType
            case 'allVoxelsMean'
                [paramMaps,fitRes,sigs,scanParamsUsed]=...
                    mm_plga.prepareForFitting(fitType,...
                    dataMatrixRep,scanParamsScaled,initialParamSeedScaled,...
                    noiseLevel,lwrBndScaled,upprBndScaled,phntmMask,diffModel);
            case 'voxelwise'
                phntmMaskReshape=reshape(phntmMask,...
                    size(dataMatrixRep,1),size(dataMatrixRep,2),1,size(dataMatrixRep,4));
                dataMatrixRep=dataMatrixRep.*repmat(phntmMaskReshape,...
                    1,1,size(dataMatrixRep,3),1,size(dataMatrixRep,5),size(dataMatrixRep,6));
                [paramMaps,fitRes,sigs,scanParamsUsed]=...
                    mm_plga.prepareForFitting(fitType,...
                    dataMatrixRep,scanParamsScaled,initialParamSeedScaled,noiseLevel,...
                    lwrBndScaled,upprBndScaled,[],diffModel);
            otherwise
                error('!!! Unrecognised fitType')
        end
        
        %% Save output
        if ~exist(strcat(subDir,'paramMaps_',saveStr,'.mat'),'file')
            save(strcat(subDir,'paramMaps_',saveStr,'.mat'),'paramMaps')
            save(strcat(subDir,'fitResiduals.mat'),'fitRes')
            save(strcat(subDir,'fitSignals.mat'),'sigs')
            save(strcat(subDir,'fitScanParams.mat'),'scanParamsUsed')
            save(strcat(subDir,'fitLowerBounds.mat'),'lwrBnd')
            save(strcat(subDir,'fitUpperBounds.mat'),'upprBnd')
        else
            disp(['paramMaps variable already exists - '...
                'saving copies'])
            save(strcat(subDir,'paramMaps_',saveStr,'_cp.mat'),'paramMaps')
            save(strcat(subDir,'fitResiduals_cp.mat'),'fitRes')
            save(strcat(subDir,'fitSignals_cp.mat'),'sigs')
            save(strcat(subDir,'fitScanParams_cp.mat'),'scanParamsUsed')
            save(strcat(subDir,'fitLowerBounds_cp.mat'),'lwrBnd')
            save(strcat(subDir,'fitUpperBounds_cp.mat'),'upprBnd')
            
        end
    end
end