- MATLAB code for microstructural modelling of PLGA phantom data (McHugh et al., Magnetic     
  Resonance in Medicine 80:147-158, 2018, doi:10.1002/mrm.27016).

- Example data provided for one week time point.

- In MATLAB, cd to mm_plga_biomimetic_phantom/

- Running process_dwi_mm.m with:

fitType = 'voxelwise' and highG_highDEL = 'include'
	-> Generates maps in Figure 4.
fitType = 'allVoxelsMean' and highG_highDEL = 'exclude'
	-> Generates two plots, showing fits for (1) fitting D, and (2) fixing D.
	-> Parameter values match those plotted in Figure 6.

- Results are saved in a newly-created subdirectory in plga_stability_210318/Analysis_day7/

- To display the parametric maps run the following commands when the voxelwise fitting has finished:

cd plga_stability_210316/Analysis_day7/mm_seed134564_100initialVals_D/voxelwise_mask_LB_1e-07_1e-10_0.01_1_UB_2.5e-05_3e-09_1_1__unscaled/
load ./paramMaps_voxelwise_mask_LB_1e-07_1e-10_0.01_1_UB_2.5e-05_3e-09_1_1.mat

mont(squeeze(paramMaps(:,:,1,:)).*1e6)
colormap(gca,parula)
title('Radius (\mum)')

mont(squeeze(paramMaps(:,:,2,:)).*1e9)
colormap(gca,parula)
title('Diffusivity (\mum^2/ms)')

mont(squeeze(paramMaps(:,:,3,:)))
colormap(gca,parula)
title('Intracellular volume fraction')
