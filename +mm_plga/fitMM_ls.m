function [fitMM_LS,res]=fitMM_LS(sig,scanParam,startingVals,lwrBnds,upprBnds,...
	plot_y_n,chooseModel)
%% Function for fitting microstructural model with either:
%  one or two diffusivities (Di=De or Di!=De)
%  Uses least squares fitting with fminsearchbnd
%% Inputs: sig - vector of signals
%          scanParam - matrix of scan parameters
%          startingValus - matrix of initial values to use in fitting
%          lwrBnd/upprBnd - vector of lower and upper bounds:
%                         use  lwrBnd=[-Inf -Inf -Inf -Inf] and
%                              upprBnd=[Inf Inf Inf Inf] for unconstrained
%                              fitting
%                         use  lwrBnd=[-Inf x -Inf -Inf] and
%                              upprBnd=[Inf x Inf Inf] to fix second
%                              parameter at x
%        plot_y_n - string indicating whether to plot ('y') or not ('n')
%        chooseModel - either 'D' or 'DiDe'
%% Outputs: fitMM_LS - fitted parameter estimates
%           res - residuals from fit

%% Fit model and collect parameter estimates
options=optimset('Display', 'off');

%% Load sphere GPA equation roots - Neuman, J Chem Phys 1974;60:4508–4511. 
rootsS=mm_plga.loadRename('./+mm_plga/gpaBesselEqnRoots_sphere.mat'); 

%% Loop over initial values and accept fit which gives lowest value of objective function
ofv(1)=Inf;
for strtValInd=1:size(startingVals,2)
    switch chooseModel
        case {'D','DiDe'}
            [fitParams,objFnVal,extflg,output]=...
                mm_plga.fminsearchbnd(@subFn,startingVals(:,strtValInd),...
                lwrBnds,upprBnds,options);
    end
    ofv(strtValInd+1)=objFnVal;
    % Update estimates if obj fun is lower
    if ofv(strtValInd+1)<min(ofv(1:strtValInd))
        switch chooseModel
            case 'D'
                rFit=fitParams(1);
                dFit=fitParams(2);
                fFit=fitParams(3);
                s0Fit=fitParams(4);
            case 'DiDe'
                rFit=fitParams(1);
                diFit=fitParams(2);
                deFit=fitParams(3);
                fFit=fitParams(4);
            otherwise
                error('!!! Unrecognised option for chooseModel')
        end
        strtValIndAccepted=strtValInd;
    else
        % If these starting values do not improve obj fn, do nothing
    end
end

%% Collect fitted parameter values
switch chooseModel
    case 'D'
        fitParamsAccepted=cat(2,rFit,dFit,fFit,s0Fit);
    case 'DiDe'
        fitParamsAccepted=cat(2,rFit,diFit,deFit,fFit);
end

%% Calculate residuals for each data point and R^2 of fit
res = sig - subFnPlot(fitParamsAccepted,scanParam);
sumSqRes = sum(res.^2);
diffFromMean = sig - mean(sig);
sumSqDiffFromMean = sum(diffFromMean.^2);
rsq = 1 - (sumSqRes./sumSqDiffFromMean);

%% Assign output
fitMM_LS=cat(1,fitParamsAccepted(:),rsq,strtValIndAccepted);
			
%% Plotting, if required
switch plot_y_n
case 'y'
    DELvals=unique(scanParam(:,2));
    C= [0    0.4470    0.7410
        0.8500    0.3250    0.0980
        0.9290    0.6940    0.1250
        0.4940    0.1840    0.5560
        0.4660    0.6740    0.1880
        0.3010    0.7450    0.9330
        0.6350    0.0780    0.1840];
	figure, hold on
    for i=1:size(DELvals(:),1)
        indexToDEL=DELvals(i);%sequenceParams(indexToDEL,2)
        [r,c,v]=find(scanParam(:,2)==DELvals(i));
        G=scanParam(r,1);
        xgrid = linspace(min(G),max(G))';
        plot(G.*1e3,sig(r),'o',xgrid.*1e3,subFnPlot(fitParamsAccepted,cat(2,xgrid,...
            repmat(scanParam(r(1),2:end),size(xgrid,1),1))),'-.',...
            'MarkerSize',10,'LineWidth',2,'Color',C(i,:));
    end
    xlabel('G (mT/m)','FontSize',22);
    ylabel('Signal intensity','FontSize',22);
    ylim([0 1])
    set(gca,'YGrid','on','box','off','FontSize',22);
    
    % Add text displaying fitted values
    switch chooseModel
        case 'D'
            text(0.15*max(G.*1e3),0.3*max(sig),sprintf(strcat(...
                'R=',num2str(fitMM_LS(1)*1e6),' \\mum',...
                '\nD=',num2str(fitMM_LS(2)*1e9), '\\mum^2/ms',...
                '\nfi=',num2str(fitMM_LS(3)),...
                '\nS0=',num2str(fitMM_LS(4)),...
                '\nR^2=',num2str(fitMM_LS(5)))),...
                'fontsize',16);  
        case 'DiDe'
            text(0.15*max(G.*1e3),0.3*max(sig),sprintf(strcat(...
                'R=',num2str(fitMM_LS(1)*1e6),' \\mum',...
                '\nDi=',num2str(fitMM_LS(2)*1e9), '\\mum^2/ms',...
                '\nDe=',num2str(fitMM_LS(3)*1e9), '\\mum^2/ms',...
                '\nfi=',num2str(fitMM_LS(4)),...
                '\nR^2=',num2str(fitMM_LS(5)))),...
                'fontsize',16);
		otherwise
			error('!!! Unrecognised chooseModel')
    end
case 'n'
    %do nothing
otherwise
    error('!!! Error - plot_y_n must be either y or n')
end

%% Signal model functions
% Sphere expression - Murday & Cotts, J Chem Phys 1968;48:4938–4945.
% Extracellular tortuosity - Price et al., Biophys J 1998;74:2259–2271.
function x=subFn(a)
    sumM=0;
    Grad=scanParam(:,1);
    DEL=scanParam(:,2);
    del=scanParam(:,3);
    gamma=scanParam(:,4);
    switch chooseModel
        case 'D'
            R=a(1);
            D=a(2);
            f=a(3);
            S0=a(4);
            alpS=rootsS./R;
            for m=1:numel(rootsS)
                aS=1/(alpS(m).^2.*(alpS(m).^2.*R.^2-2));
                bS=(2.*del)./(alpS(m).^2.*D);
                cS=(2+exp(-alpS(m).^2.*D.*(DEL-del)) - 2.*exp(-alpS(m).^2.*D.*del) -2.*exp(-alpS(m).^2.*D.*DEL) + exp(-alpS(m).^2.*D.*(DEL+del)) )./((alpS(m).^2.*D).^2);
                exprM=aS.*(bS-cS);
                sumM=sumM+exprM;
            end
            murdayCotts=exp(-2.*(gamma.^2).*(Grad.^2).*sumM);
            signalModel=S0.*((f.*murdayCotts)+...
                ((1-f).*exp(-(((Grad.*del.*gamma).^2).*(DEL-del./3)).*(D./(1+f./2)))));
        case 'DiDe'
            R=a(1);
            Di=a(2);
            De=a(3);
            f=a(4);
            alpS=rootsS./R;
            for m=1:numel(rootsS)
                aS=1/(alpS(m).^2.*(alpS(m).^2.*R.^2-2));
                bS=(2.*del)./(alpS(m).^2.*Di);
                cS=(2+exp(-alpS(m).^2.*Di.*(DEL-del)) - 2.*exp(-alpS(m).^2.*Di.*del) -2.*exp(-alpS(m).^2.*Di.*DEL) + exp(-alpS(m).^2.*Di.*(DEL+del)) )./((alpS(m).^2.*Di).^2);
                exprM=aS.*(bS-cS);
                sumM=sumM+exprM;
            end
            murdayCotts=exp(-2.*(gamma.^2).*(Grad.^2).*sumM);
            signalModel=1.*((f.*murdayCotts)+...
                ((1-f).*exp(-(((Grad.*del.*gamma).^2).*(DEL-del./3)).*(De./(1+f./2)))));        
        otherwise
			error('!!! Unrecognised chooseModel') 
    end
	% Least squares objective function
	x=sum((sig-signalModel).^2);   
end % End subFn

%% As above but for plotting and residuals calculations
function F=subFnPlot(a,b)
    sumM=0;
    Grad=b(:,1);
    DEL=b(:,2);
    del=b(:,3);
    gamma=b(:,4);
    switch chooseModel
        case 'D'
            R=a(1);
            D=a(2);
            f=a(3);
            S0=a(4);
            alpS=rootsS./R;
            for m=1:numel(rootsS)
                aS=1/(alpS(m).^2.*(alpS(m).^2.*R.^2-2));
                bS=(2.*del)./(alpS(m).^2.*D);
                cS=(2+exp(-alpS(m).^2.*D.*(DEL-del)) - 2.*exp(-alpS(m).^2.*D.*del) -2.*exp(-alpS(m).^2.*D.*DEL) + exp(-alpS(m).^2.*D.*(DEL+del)) )./((alpS(m).^2.*D).^2);
                exprM=aS.*(bS-cS);
                sumM=sumM+exprM;
            end
            murdayCotts_=exp(-2.*(gamma.^2).*(Grad.^2).*sumM);
            F=S0.*((f.*murdayCotts_)+...
                ((1-f).*exp(-(((Grad.*del.*gamma).^2).*(DEL-del./3)).*(D./(1+f./2)))));
        case 'DiDe'
            R=a(1);
            Di=a(2);
            De=a(3);
            f=a(4);
            alpS=rootsS./R;
            for m=1:numel(rootsS)
                aS=1/(alpS(m).^2.*(alpS(m).^2.*R.^2-2));
                bS=(2.*del)./(alpS(m).^2.*Di);
                cS=(2+exp(-alpS(m).^2.*Di.*(DEL-del)) - 2.*exp(-alpS(m).^2.*Di.*del) -2.*exp(-alpS(m).^2.*Di.*DEL) + exp(-alpS(m).^2.*Di.*(DEL+del)) )./((alpS(m).^2.*Di).^2);
                exprM=aS.*(bS-cS);
                sumM=sumM+exprM;
            end
            murdayCotts_=exp(-2.*(gamma.^2).*(Grad.^2).*sumM);
            F=1.*((f.*murdayCotts_)+...
                ((1-f).*exp(-(((Grad.*del.*gamma).^2).*(DEL-del./3)).*(De./(1+f./2)))));
    end
end % End subFnPlot

end % End main function