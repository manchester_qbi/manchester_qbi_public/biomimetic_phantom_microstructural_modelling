function [paramMaps,fitResiduals,signalsUsed,scanParamsUsed]=prepareForFitting(...
    fitType,dataMatrix,scanParams,initialParamSeed,noiseLevel,lwrBnd,upprBnd,...
    mask,diffModel)
%% Function which organises data and performs normalisation before fitting
% Inputs: fitType - string specifying 'voxelwise' or 'allVoxelsMean'
%         dataMatrix - 6D matrix with diffusion images
%         scanParams - matrix specifying scan parameters
%         initialParamSeed - matrix of initial values to use in fitting
%         noiseLevel - Rician noise from background
%         lwrBnd,upprBnd - lower and upper bounds for fit constraints
%         mask - binary mask specifying voxels to use when averaging
%         diffModel - string specifying model to fit, 'D' or 'De'
% Outputs: paramMaps - matrix of fitted parameter maps
%          fitResiduals - for allVoxelsAveraged, vector of fit residuals
%          signalsUsed - for allVoxelsAveraged, vector of signals used in fitting
%          scanParamsUsed - for allVoxelsAveraged, matrix of scan parameters used in fitting

switch fitType
    case 'voxelwise' % Fit voxelwise signals
        
        % Pre-allocate output
        switch diffModel
            case {'D','DiDe'}
                paramMaps=...
                    zeros(size(dataMatrix,1),size(dataMatrix,2),7,size(dataMatrix,4));
            otherwise
                error('!!! Unrecognised diffModel')
        end
        
        % Loop over slices and fit voxel wise
        for sliceInd=1:size(dataMatrix,4)
            
            % Get this slice and corresponding noise
            thisSliceAllDir=squeeze(dataMatrix(:,:,:,sliceInd,:,:));
            sliceNoiseLevel=noiseLevel(sliceInd);
            
            % Average across directions
            thisSliceMean=squeeze(mean(thisSliceAllDir,4));
            
            % Normalise to G0
            for DELind=1:size(dataMatrix,6)
                for gradInd=1:size(dataMatrix,3)
                    thisSliceNorm(:,:,gradInd,DELind)=...
                        thisSliceMean(:,:,gradInd,DELind)./...
                        thisSliceMean(:,:,1,DELind);
                end
            end
            
            % Pass to fitting subfunction
            paramMaps(:,:,:,sliceInd)=fitSubFn(thisSliceNorm,thisSliceMean,...
                scanParams,initialParamSeed,sliceNoiseLevel,...
                lwrBnd,upprBnd,diffModel,'n');
        end
        
    case 'allVoxelsMean' % Fit mean of all masked voxels
        
        % First check that the mask was supplied as an argument
        if isempty(mask)
            error('Error - mask file must be supplied for allVoxelsMean fitting')
        end
        
        % Create empty array to hold all voxel signals
        collectAll=[];
        
        % Average noise level over all slices
        avNoiseLevel=mean(noiseLevel(:));
        
        % Loop over slices
        for sliceInd=1:size(dataMatrix,4)
            
            % Index for collectAll update
            i1=size(collectAll,1)+1;
            
            % Get this slice
            thisSliceAllDir=squeeze(dataMatrix(:,:,:,sliceInd,:,:));
            
            % Apply mask
            thisSliceMask=mask(:,:,sliceInd);
            maskRep=repmat(thisSliceMask,[1 1 ...
                size(thisSliceAllDir,3) size(thisSliceAllDir,4) ...
                size(thisSliceAllDir,5)]);
            applyMask=thisSliceAllDir.*maskRep;
            
            % Collect signals for all directions for this slice, as a
            % function of G and DEL
            for gradInd=1:size(applyMask,3)
                for DELind=1:size(applyMask,5)
                    thisSliceAll=applyMask(:,:,gradInd,:,DELind);
                    i2=i1-1+size(thisSliceAll(:),1);
                    collectAll(i1:i2,gradInd,DELind)=thisSliceAll(:);
                end
            end
        end
        
        % Check that we've collected the expected total number of voxels
        % NOTE: this includes phantom voxels and background!
        numExpectedVoxels=size(dataMatrix,1)*size(dataMatrix,2)*...
            size(dataMatrix,4)*size(dataMatrix,5);
        numCollectedVoxels=size(collectAll,1);
        if numExpectedVoxels==numCollectedVoxels
            % if we're here we're ok
        else
            disp(strcat('numExpectedVoxels=',num2str(numExpectedVoxels)))
            disp(strcat('numCollectdeVoxels=',num2str(numCollectedVoxels)))
            error('!!! Error - incorrect total number of voxels collected')
        end
        
        % Average over all voxels
        for gradInd=1:size(applyMask,3)
            for DELind=1:size(applyMask,5)
                thisSigVec=squeeze(collectAll(:,gradInd,DELind));
                thisSigVecMaskedVox=thisSigVec(thisSigVec>0);
                % Check we're averaging over the expected number of masked
                % voxels
                if size(thisSigVecMaskedVox,1)==...
                        size(find(mask(:)==1),1)*size(dataMatrix,5)
                    meanTemp(gradInd,DELind)=mean(thisSigVecMaskedVox);
                else
                    size(thisSigVecMaskedVox,1)
                    size(find(mask(:)==1),1)
                    error('!!! Error - incorrect number of masked voxels')
                end
            end
        end
        
        % Reshape so it can be passed to same code as voxelwise fitting,
        % which assumes we're working with images
        thisMean=reshape(meanTemp,1,1,size(applyMask,3),...
            size(applyMask,5));
        
        % Normalise to G0
        for DELind=1:size(applyMask,5)
            for gradInd=1:size(applyMask,3)
                thisNorm(:,:,gradInd,DELind)=...
                    thisMean(:,:,gradInd,DELind)./...
                    thisMean(:,:,1,DELind);
            end
        end
        
        %{
        testOut.collectAll=collectAll;
        testOut.thisNorm=thisNorm;
        testOut.thisMean=thisMean;
        testOut.avNoiseLevel=avNoiseLevel;
        testOut.lb=lwrBnd;
        testOut.ub=upprBnd;
        %}
        
        % Pass to fitting subfunction
        [paramMaps,residuals,signals,scanParamsUsed]=fitSubFn(thisNorm,...
            thisMean,scanParams,initialParamSeed,avNoiseLevel,...
            lwrBnd,upprBnd,diffModel,'y');
        
    otherwise
        error('!!! Unrecognised fitType')
end

%% Assign output
switch fitType
    case 'voxelwise'
        fitResiduals=[];
        signalsUsed=[];
        scanParamsUsed=[];
    case 'allVoxelsMean'
        fitResiduals=residuals;
        signalsUsed=signals;
end

end % End main function

%% Subfunction which reshapes signal matrices and
%  calls fitVoxelwise
function [fitSubFn,fitRes,sigUsed,scanParamsUsed]=...
    fitSubFn(thisSliceNorm,thisSliceMean,...
    scanParams,initialParamSeed,sliceNoiseLevel,...
    lwrBnd,upprBnd,modelToUse,plotYesNo)

% Reshape to 3D matrix - format: (:,:,firstG & firstDEL, secondG &
% firstDEL, thirdG & firstDEL...)
signalsToFit=reshape(thisSliceMean,size(thisSliceMean,1),...
    size(thisSliceMean,2),size(thisSliceMean,3)*size(thisSliceMean,4));
signalsToFitNorm=reshape(thisSliceNorm,size(thisSliceNorm,1),...
    size(thisSliceNorm,2),size(thisSliceNorm,3)*size(thisSliceNorm,4));

[fitSubFn,fitRes,sigUsed,scanParamsUsed]=...
    mm_plga.fitMM_voxelwise(signalsToFit,signalsToFitNorm,...
    scanParams,initialParamSeed,sliceNoiseLevel,...
    lwrBnd,upprBnd,modelToUse,plotYesNo);

end % End of subfunction