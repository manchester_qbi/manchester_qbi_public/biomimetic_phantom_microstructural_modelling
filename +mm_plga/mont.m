function mont = mont(ims,varargin)
%% Displays a 3D matrix as a montage (will also handle 2D matrix)
%  Inputs: - ims: 3D matrix of images or 2D single image
%          - varargin: optional argument where upper limit of colormap can
%            be specified; o.w. a default limit of 1*maximum is used
%          - second optional argument for passing size of
%            resulting montage [rows,cols]
%  Output: - mont: handle to figure

% For comparing images with diferent resolutions, use imresize when calling
% mont, i.e to compare 64x64 and 128x128 use:
% mont(my128by128) and mont(imresize(my64by64,2))

%% Check we have a 2D or 3D matrix, and reshape image in format needed
%  for montage function
if size(size(ims),2)==2
    imsForMontage=reshape(ims,size(ims,1),size(ims,2),1,1);
elseif size(size(ims),2)==3
    imsForMontage=reshape(ims,size(ims,1),size(ims,2),1,size(ims,3));
else
    error('!!! Error - ims must be a 2D or 3D matrix')
end

%% Set colourbar upper limit
if nargin==1
    cbLim=1*max(imsForMontage(:));
    % Adjust if cbLim==0, as this will throw an error when trying to
    % display
    if cbLim==0
        cbLim=1;
        disp('Colorbar upper limit is 0 - resetting to 1')
    end
elseif nargin==2 || nargin==3
    % Check if colourmap limit was passed as empty
    if isempty(varargin{1})
        cbLim=1*max(imsForMontage(:));
        % Adjust if cbLim==0, as this will throw an error when trying to
        % display
        if cbLim==0
            cbLim=1;
            disp('Colorbar upper limit is 0 - resetting to 1')
        end
    else
        cbLim=varargin{1};
    end
else
    error('!!! Error - function call must have one, two, or three arguments')
end

%% Display
figure
if nargin==1 || nargin==2
    montage(imsForMontage,'displayrange',[0 cbLim])
elseif nargin==3
    montage(imsForMontage,'displayrange',[0 cbLim],'size',varargin{2})
end
set(gcf,'Position',[9 243 1179 1102])
set(gca, 'Units', 'normalized', 'Position', [0.03 0.03 0.87 0.87])