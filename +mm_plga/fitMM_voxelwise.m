function [fittedParams,fitRes,sToUse,...
    paramsToUse,weightsThisVoxToUse]=fitMM_voxelwise(ims,imsNorm,...
    scanParams,initialParams,riceNoiseStd,lb,ub,diffModel,plotYesNo)
%% Function for fitting microstructural model on a voxel-wise basis
% Inputs: ims - image signal intensities
%         imsNorm - normalised signal intensities (normalised to G=0)
%         scanParams - matrix of scan parameters
%         initalParams - matrix of initial parameters for fitting
%         riceNoiseStd - Rician noise values
%         lb,ub - lower and upper fit bounds
%         diffModel - string specifying model to fit

%% Noise - Gudbjartsson H, Patz S. Magn Reson Med 1995; 34:910–914.
noiseMean=riceNoiseStd.*sqrt(pi/2);

%% Check that number of signals matches number of scan parameters
if size(imsNorm,3)~=size(scanParams,1)
    error(['!!! Error - number of signals and number of scan '...
        'parameters do not match']);
end
   
%% Initialise output maps
switch diffModel
	case 'D'
    	rMap=zeros(size(ims,1),size(ims,2));
    	dMap=zeros(size(ims,1),size(ims,2));
    	fMap=zeros(size(ims,1),size(ims,2));
    	s0Map=zeros(size(ims,1),size(ims,2));
    	rsqMap=zeros(size(ims,1),size(ims,2));
    	strtValMap=zeros(size(ims,1),size(ims,2));
    	numSig=zeros(size(ims,1),size(ims,2));
    	fitRes=zeros(size(ims,1),size(ims,2));
    	sToUse=zeros(size(ims,1),size(ims,2));
    	paramsToUse=zeros(size(ims,1),size(ims,2));
    case 'DiDe'
        rMap=zeros(size(ims,1),size(ims,2));
        diMap=zeros(size(ims,1),size(ims,2));
        deMap=zeros(size(ims,1),size(ims,2));
        fMap=zeros(size(ims,1),size(ims,2));
        rsqMap=zeros(size(ims,1),size(ims,2));
        strtValMap=zeros(size(ims,1),size(ims,2));
        numSig=zeros(size(ims,1),size(ims,2));
        fitRes=zeros(size(ims,1),size(ims,2));
        sToUse=zeros(size(ims,1),size(ims,2));
        paramsToUse=zeros(size(ims,1),size(ims,2));
	otherwise
		error('!!! Unrecognised diffModel')
end

%% Get signals to fit, and pass to fitMM_LS
for i=1:size(ims,1)
    for j=1:size(ims,2)
        s=squeeze(ims(i,j,:));
        % If any signals are 0, don't perform fitting
        if all(s(:))==1
        	sNorm=squeeze(imsNorm(i,j,:));       
        	% Perform fitting if we're not in the noise
        	% 3xricianNoiseStd mostly excludes the noise
        	if s(1)>3*riceNoiseStd
            	% Exclude signals where intensity is lower than 2*noise ROI
           	 	% mean (Portnoy et al. MRM 2013), and get corresponding 
				% normalised signals
            	sToUse=sNorm(s>2*noiseMean);
            	paramsToUse=scanParams(s>2*noiseMean,:);
            	switch diffModel
                	case 'D'
                    	% pass norm signals and corresponding scan parameters to
                    	% fitMurdayCotts_LS_and_MLE.m
                    	[paramEstimatesLS, fitRes]=mm_plga.fitMM_ls(...
                        	sToUse,paramsToUse,initialParams,lb,ub,plotYesNo,'D'); 
                    	% collect each parameter for map
                    	rMap(i,j)=paramEstimatesLS(1);
                   	 	dMap(i,j)=paramEstimatesLS(2);
                    	fMap(i,j)=paramEstimatesLS(3);
                    	s0Map(i,j)=paramEstimatesLS(4);
                    	rsqMap(i,j)=paramEstimatesLS(5);
                    	strtValMap(i,j)=paramEstimatesLS(6);
                    	numSig(i,j)=numel(sToUse);
                	case 'DiDe'
                    	% Pass norm signals and corresponding scan parameters to
                    	% fitMM_LS.m
                    	[paramEstimatesLS, fitRes]=mm_plga.fitMM_ls(...
                    		sToUse,paramsToUse,initialParams,lb,ub,plotYesNo,'DiDe');
                    	% collect each parameter for map
                    	rMap(i,j)=paramEstimatesLS(1);
                    	diMap(i,j)=paramEstimatesLS(2);
                    	deMap(i,j)=paramEstimatesLS(3);
                    	fMap(i,j)=paramEstimatesLS(4);
                    	rsqMap(i,j)=paramEstimatesLS(5);
                    	strtValMap(i,j)=paramEstimatesLS(6);
                    	numSig(i,j)=numel(sToUse);
					otherwise
						error('!!! Unrecognised diffModel')
            	end
        	else
            	switch diffModel
            		case 'D'
                		rMap(i,j)=0;
                		dMap(i,j)=0;
                		fMap(i,j)=0;
                		s0Map(i,j)=0;
                		rsqMap(i,j)=0;
                		strtValMap(i,j)=0;
                		numSig(i,j)=0;
                		fitRes(i,j)=NaN;
                		sToUse(i,j)=NaN;
                		paramsToUse(i,j)=NaN;
                	case 'DiDe'
                   		rMap(i,j)=0;
                    	diMap(i,j)=0;
                    	deMap(i,j)=0;
                    	fMap(i,j)=0;
                    	rsqMap(i,j)=0;
                    	strtValMap(i,j)=0;
                    	numSig(i,j)=0;
                    	fitRes(i,j)=NaN;
                    	sToUse(i,j)=NaN;
                    	paramsToUse(i,j)=NaN;
					otherwise
						error('!!! Unrecognised diffModel')
            	end
        	end
        else
            switch diffModel
                case 'D'
                    rMap(i,j)=0;
                    dMap(i,j)=0;
                    fMap(i,j)=0;
                    s0Map(i,j)=0;
                    rsqMap(i,j)=0;
                    strtValMap(i,j)=0;
                    numSig(i,j)=0;
                    fitRes(i,j)=NaN;
                    sToUse(i,j)=NaN;
                    paramsToUse(i,j)=NaN;
                case 'DiDe'
                    rMap(i,j)=0;
                    diMap(i,j)=0;
                    deMap(i,j)=0;
                    fMap(i,j)=0;
                    rsqMap(i,j)=0;
                    strtValMap(i,j)=0;
                    numSig(i,j)=0;
                    fitRes(i,j)=NaN;
                    sToUse(i,j)=NaN;
                    paramsToUse(i,j)=NaN;
				otherwise
					error('!!! Unrecognised diffModel')
            end
        end  
    end
end

switch diffModel
	case 'D'
    	fittedParams=cat(3,rMap,dMap,fMap,s0Map,...
        	rsqMap,strtValMap,numSig);
    case 'DiDe'
        fittedParams=cat(3,rMap,diMap,deMap,fMap,...
            rsqMap,strtValMap,numSig);
end